protocol Level{
	
	func createAvatar() -> Avatar    
}


class LevelOne :Level{
	func createAvatar() -> Avatar{
		return BasicAvatar()
	}
}

class LevelTwo :Level{
	func createAvatar() -> Avatar{
		return MiddleAvatar()
	}
}

class LevelThree :Level{
	func createAvatar() -> Avatar{
		return AdvancedAvatar()
	}
}

protocol Avatar{
	var x: Int { get set }
    var y: Int { get set }
    var name : String {get}
	func walkRigth()
	func walkLeft()
	func jump()
    init ()
}

class BasicAvatar : Avatar{
	var x: Int
    var y: Int
    var name : String
	func walkRigth(){
		self.x = self.x + 1 
	}
	func walkLeft(){
		self.x = self.x - 1 
	}
	func jump(){
		self.x = self.y + 1 
	}
    required init() {
        self.x = 0
        self.y = 0
        self.name = "Gnome"
    }
   
}

class MiddleAvatar : Avatar{
	var x: Int 
    var y: Int 
    var name : String
	func walkRigth(){
		self.x = self.x + 3 
	}
	func walkLeft(){
		self.x = self.x - 3 
	}
	func jump(){
		self.x = self.y + 3 
	}

    required init() {
        self.x = 100
        self.y = 100
        self.name = "Elf"
    }
}

class AdvancedAvatar : Avatar{
	var x: Int
    var y: Int
    var name : String
	func walkRigth(){
		self.x = self.x + 5 
	}
	func walkLeft(){
		self.x = self.x - 5 
	}
	func jump(){
		self.x = self.y + 5 
	}
    required init() {
        self.x = 200
        self.y = 200
        self.name = "Ogre"
    }
}

class Game{

    func whatLevel(_ score: Int){ 
      if(score >= 0 && score <= 100){
          let level = LevelOne()
          let avatar = level.createAvatar()
          print("Hello!! I'm a ", avatar.name)
      }else if(score >= 101 && score <= 500 ){
          let level = LevelTwo()
           let avatar =  level.createAvatar()
           print("Hello!! I'm a ", avatar.name)
      }else{
          let level = LevelThree()
          let avatar = level.createAvatar()
          print("Hello!! I'm a ", avatar.name)
      }

    }
}


    let game1 = Game()
    game1.whatLevel(10)


